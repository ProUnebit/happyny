// Custom modules
import { Map_SkatingRinks } from './components/MapNY1'
import { Map_Salutes } from './components/MapNY2'
import { Game_Movies } from './components/GameNY'

function DOMIsReady() {

    if (document.getElementById('map--skating-rinks')) {

        Map_SkatingRinks.mapInit('https://raw.githubusercontent.com/ProUnebit/Reminder_ToMe_Reminder/master/skating-rinks-december-2018.json')
    }

    if (document.getElementById('map--salutes')) {

        Map_Salutes.mapInit('https://raw.githubusercontent.com/ProUnebit/Reminder_ToMe_Reminder/master/salutes-december-2018.json')
    }

    if (document.getElementById('game--movies')) {

        Game_Movies.gameInit()
    }
}

document.addEventListener('DOMContentLoaded', DOMIsReady);
