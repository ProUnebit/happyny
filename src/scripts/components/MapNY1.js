import { skatingRinksData } from './skatingRingsData'

import DG from '2gis-maps'
import '../../common_libraries/jquery-global'
// import '@fancyapps/fancybox'
// import "lightgallery.js";

var jQ = jQuery.noConflict();

class MapNY1 {
    constructor(id) {
        this.id = id
        this.data;
        this.currentPopup;
    }

    async mapInit(url) {

        // console.log('map1 init')

        // try {
        //     this.data = await this.mapGetData(url)
        //
        // } catch(err) {
        //     console.warn('Неудачный запрос данных: Катки', err)
        // }

        this.data = skatingRinksData

        await this.mapMarking(this.id)

        // lightGallery((document.getElementById('lightgallery')))
    }

    mapGetData(url) {

        return fetch(url).then(res => res.json()).then(data => data)
    }

    mapMarking(id) {

        let { data } = this

        DG
        .then(() => DG.plugin('https://2gis.github.io/mapsapi/vendors/Leaflet.markerCluster/leaflet.markercluster-src.js'))
        .then(() => {

            let map,
            coordinates = [],
            markerGroup = DG.markerClusterGroup(),
            myIcon = DG.icon({
                    iconUrl: 'data:image/svg+xml;base64,PHN2ZyBoZWlnaHQ9IjUxMnB0IiB2aWV3Qm94PSIwIC0xMyA1MTIuMDAxMDEgNTEyIiB3aWR0aD0i%0D%0ANTEycHQiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PHBhdGggZD0ibTM0MC4z%0D%0ANzEwOTQgMzYxLjczMDQ2OWg2OS40NTcwMzF2ODguOTUzMTI1aC02OS40NTcwMzF6bTAgMCIgZmls%0D%0AbD0iI2RhZjNmZSIvPjxwYXRoIGQ9Im00Mi4wMTE3MTkgMzYxLjczMDQ2OWg1Mi41MDc4MTJ2ODgu%0D%0AOTUzMTI1aC01Mi41MDc4MTJ6bTAgMCIgZmlsbD0iI2RhZjNmZSIvPjxwYXRoIGQ9Im0xMjguOTc2%0D%0ANTYyIDMyNS44MTI1LTE1LjA5NzY1NiA1Ny41OTM3NWgtOTYuNjI4OTA2bC05LjY5NTMxMi01Ny41%0D%0AOTM3NSAzMC4xOTkyMTgtOC40NDUzMTIgMjMuNzc3MzQ0LTYuNjUyMzQ0em0wIDAiIGZpbGw9IiM0%0D%0AYTVjNjYiLz48cGF0aCBkPSJtNTAwLjgyMDMxMiAzMjguMDcwMzEyYzAgOS41MTE3MTktMi40ODQz%0D%0ANzQgMTguNjQwNjI2LTYuOTA2MjUgMjYuNjQ0NTMyLTkuMzAwNzgxIDE2LjgzMjAzMS0yNy4xNjc5%0D%0ANjggMjguNjkxNDA2LTQ4LjU1MDc4MSAyOC42OTE0MDZoLTEzOS4zOTQ1MzFjLTIwLjI1MzkwNiAw%0D%0ALTQwLjIzNDM3NS00LjcyMjY1Ni01OC4zNDM3NS0xMy43ODEyNWwtNjAuMDcwMzEyLTMwLjAzMTI1%0D%0AYy0xOC4xMTcxODgtOS4wNTg1OTQtMzguMTAxNTYzLTEzLjc4MTI1LTU4LjM1NTQ2OS0xMy43ODEy%0D%0ANWgtMTIxLjY0NDUzMXYtMTAyLjYyODkwNmMwLTI0LjcxMDkzOCA1LjI1MzkwNi00OS4xNDQ1MzIg%0D%0AMTUuNDEwMTU2LTcxLjY3MTg3NSA1LjA4NTkzNy0xMS4yNjU2MjUgOC45Mjk2ODctMjMuMDAzOTA3%0D%0AIDExLjUxNTYyNS0zNS4wMjM0MzggMi41ODk4NDMtMTIuMDExNzE5IDMuODk4NDM3LTI0LjI5Mjk2%0D%0AOSAzLjg5ODQzNy0zNi42NTIzNDN2LTcyLjI4OTA2M2gyMDAuNTE1NjI1YzE4LjIxMDkzOCAxNzgu%0D%0AMzgyODEzIDEyMC4zMzk4NDQgMjI2LjA1ODU5NCAxODUuMjg5MDYzIDIzOC4xNDg0MzcgMzQuOTI5%0D%0ANjg3IDYuNSA2My4zOTA2MjUgMzEuNzc3MzQ0IDc0LjA1MDc4MSA2NS42NzE4NzYgMS43NjE3MTkg%0D%0ANS41OTc2NTYgMi41ODU5MzcgMTEuMjE0ODQzIDIuNTg1OTM3IDE2LjcwMzEyNHptMCAwIiBmaWxs%0D%0APSIjZGFmM2ZlIi8+PHBhdGggZD0ibTY4LjU3ODEyNSA3LjU0Njg3NXY3Mi4yODkwNjNjMCAxMi4z%0D%0ANTkzNzQtMS4zMDg1OTQgMjQuNjQwNjI0LTMuODk0NTMxIDM2LjY1MjM0My0yLjU4OTg0NCAxMi4w%0D%0AMTk1MzEtNi40MzM1OTQgMjMuNzU3ODEzLTExLjUxNTYyNSAzNS4wMjM0MzgtMTAuMTYwMTU3IDIy%0D%0ALjUyNzM0My0xNS40MTQwNjMgNDYuOTYwOTM3LTE1LjQxNDA2MyA3MS42NzE4NzV2MTAyLjYyODkw%0D%0ANmgtMzAuMTk5MjE4di0xMDIuNjI4OTA2YzAtMjQuNzEwOTM4IDUuMjUzOTA2LTQ5LjE0NDUzMiAx%0D%0ANS40MTAxNTYtNzEuNjcxODc1IDUuMDg1OTM3LTExLjI2NTYyNSA4LjkyOTY4Ny0yMy4wMDM5MDcg%0D%0AMTEuNTE1NjI1LTM1LjAyMzQzOCAyLjU4OTg0My0xMi4wMTE3MTkgMy44OTg0MzctMjQuMjkyOTY5%0D%0AIDMuODk4NDM3LTM2LjY1MjM0M3YtNzIuMjg5MDYzem0wIDAiIGZpbGw9IiM5ZWMyY2MiLz48cGF0%0D%0AaCBkPSJtNDkzLjkxNDA2MiAzNTQuNzE0ODQ0Yy05LjI4OTA2MiAxNi44MzIwMzEtMjcuMTYwMTU2%0D%0AIDI4LjY5MTQwNi00OC41NTA3ODEgMjguNjkxNDA2aC0xMzkuMzgyODEyYy0yMC4yNTM5MDcgMC00%0D%0AMC4yMzgyODEtNC43MjI2NTYtNTguMzU1NDY5LTEzLjc4MTI1bC02MC4wNzAzMTItMzAuMDMxMjVj%0D%0ALTE4LjExNzE4OC05LjA1ODU5NC0zOC4wODk4NDQtMTMuNzgxMjUtNTguMzU1NDY5LTEzLjc4MTI1%0D%0AaC0xMjEuNjQ0NTMxdi0yOC42ODc1aDEyMS42NTYyNWMyMC4yNTM5MDYgMCA0MC4yMzQzNzQgNC43%0D%0AMTg3NSA1OC4zNTU0NjggMTMuNzgxMjVsNjAuMDY2NDA2IDMwLjAyNzM0NGMxOC4xMDkzNzYgOS4w%0D%0ANjI1IDM4LjA5Mzc1IDEzLjc4MTI1IDU4LjM0NzY1NyAxMy43ODEyNXptMCAwIiBmaWxsPSIjNGE1%0D%0AYzY2Ii8+PHBhdGggZD0ibTQyLjAxMTcxOSAzODMuNDA2MjVoLTI0Ljc2MTcxOWwtOS42OTUzMTIt%0D%0ANTcuNTkzNzV2LTI4LjY4NzVoMzAuMTk5MjE4djI4LjY4NzV6bTAgMCIgZmlsbD0iIzM3NDk1MSIv%0D%0APjxwYXRoIGQ9Im00MzAuOTY0ODQ0IDQ3OS4zNDc2NTZoLTQyMy40MTQwNjN2LTUwLjMzMjAzMWg0%0D%0AMjMuNDE0MDYzYzEyLjc2NTYyNSAwIDIzLjE1MjM0NC0xMC4zODY3MTkgMjMuMTUyMzQ0LTIzLjE1%0D%0AMjM0NGg1MC4zMzIwMzFjMCA0MC41MTk1MzEtMzIuOTY0ODQ0IDczLjQ4NDM3NS03My40ODQzNzUg%0D%0ANzMuNDg0Mzc1em0wIDAiIGZpbGw9IiNkYWYzZmUiLz48cGF0aCBkPSJtMjE2LjU2MjUgNDQuMDIz%0D%0ANDM4YzIuNzM0Mzc1LTEuNzk2ODc2IDMuOTg0Mzc1LTUuMzQzNzUgMy4wMjczNDQtOC40NjQ4NDQt%0D%0ALjkzMzU5NC0zLjA1MDc4Mi0zLjc3NzM0NC01LjI2MTcxOS02Ljk4MDQ2OS01LjM1NTQ2OS02LjQw%0D%0ANjI1LS4xODM1OTQtMTAuMTU2MjUgNy41MzkwNjMtNS45ODQzNzUgMTIuNDQ5MjE5IDIuNDU3MDMx%0D%0AIDIuODkwNjI1IDYuNzg5MDYyIDMuNDQ1MzEyIDkuOTM3NSAxLjM3MTA5NHptMCAwIi8+PHBhdGgg%0D%0AZD0ibTMwOC40MDIzNDQgMjMyLjE2MDE1NmMyLjE3MTg3NSAwIDQuMzI4MTI1LS45MzM1OTQgNS44%0D%0AMjgxMjUtMi43NDYwOTQgMi42NDQ1MzEtMy4yMTA5MzcgMi4xOTE0MDYtNy45NzI2NTYtMS4wMTk1%0D%0AMzEtMTAuNjMyODEyLTMuMjE4NzUtMi42NDQ1MzEtNy45ODA0NjktMi4xOTE0MDYtMTAuNjI4OTA3%0D%0AIDEuMDE5NTMxLTIuNjU2MjUgMy4yMTg3NS0yLjIwMzEyNSA3Ljk3MjY1NyAxLjAxNTYyNSAxMC42%0D%0AMjg5MDcgMS4zOTg0MzggMS4xNjc5NjggMy4xMTMyODIgMS43MzA0NjggNC44MDQ2ODggMS43MzA0%0D%0ANjh6bTAgMCIvPjxwYXRoIGQ9Im0zNDMuODY3MTg4IDI1Ni4yODkwNjJjMi42NTYyNSAwIDUuMjM0%0D%0AMzc0LTEuMzk4NDM3IDYuNjEzMjgxLTMuODgyODEyIDIuMDIzNDM3LTMuNjU2MjUuNzAzMTI1LTgu%0D%0AMjQ2MDk0LTIuOTQ5MjE5LTEwLjI2OTUzMS0zLjY0NDUzMS0yLjAyMzQzOC04LjIzNDM3NS0uNjk1%0D%0AMzEzLTEwLjI1NzgxMiAyLjk0OTIxOS0yLjAyMzQzOCAzLjY0NDUzMS0uNzA3MDMyIDguMjQ2MDkz%0D%0AIDIuOTQ5MjE4IDEwLjI1NzgxMiAxLjE1NjI1LjY0NDUzMSAyLjQwNjI1Ljk0NTMxMiAzLjY0NDUz%0D%0AMi45NDUzMTJ6bTAgMCIvPjxwYXRoIGQ9Im0yNzggMjAxLjg5MDYyNWMxLjcxMDkzOCAwIDMuNDIx%0D%0AODc1LS41NzQyMTkgNC44Mzk4NDQtMS43NjE3MTkgMy4xOTE0MDYtMi42NzU3ODEgMy42MTcxODct%0D%0ANy40Mzc1LjkzNzUtMTAuNjI4OTA2LTIuNjY3OTY5LTMuMjAzMTI1LTcuNDI5Njg4LTMuNjI1LTEw%0D%0ALjYyODkwNi0uOTQ5MjE5LTMuMjAzMTI2IDIuNjc5Njg4LTMuNjI1IDcuNDQxNDA3LS45NDkyMTkg%0D%0AMTAuNjQwNjI1IDEuNSAxLjc4MTI1IDMuNjM2NzE5IDIuNjk5MjE5IDUuODAwNzgxIDIuNjk5MjE5%0D%0Aem0wIDAiLz48cGF0aCBkPSJtMjM0LjU4MjAzMSAxMjguMTMyODEyYy45Mzc1IDAgMS44OTA2MjUt%0D%0ALjE3MTg3NCAyLjgwODU5NC0uNTQyOTY4IDMuODc1LTEuNTUwNzgyIDUuNzQ2MDk0LTUuOTQ5MjE5%0D%0AIDQuMTk5MjE5LTkuODE2NDA2LTEuNTUwNzgyLTMuODY3MTg4LTUuOTUzMTI1LTUuNzQ2MDk0LTku%0D%0AODE2NDA2LTQuMTk5MjE5LTMuODc1IDEuNTUwNzgxLTUuNzUgNS45NDkyMTktNC4xOTkyMTkgOS44%0D%0AMTY0MDYgMS4xNzk2ODcgMi45NDkyMTkgNC4wMTk1MzEgNC43NDIxODcgNy4wMDc4MTIgNC43NDIx%0D%0AODd6bTAgMCIvPjxwYXRoIGQ9Im0yMjEuMTUyMzQ0IDg3LjM1MTU2MmMuNjMyODEyIDAgMS4yODkw%0D%0ANjItLjA3ODEyNCAxLjkzMzU5NC0uMjUgNC4wMjczNDMtMS4wNjY0MDYgNi40MzM1OTMtNS4xOTUz%0D%0AMTIgNS4zNjMyODEtOS4yMzA0NjgtMS4wNTQ2ODgtNC4wMjczNDQtNS4xOTE0MDctNi40MzM1OTQt%0D%0AOS4yMTg3NS01LjM2NzE4OC00LjAzOTA2MyAxLjA2NjQwNi02LjQ0MTQwNyA1LjE5NTMxMy01LjM3%0D%0ANSA5LjIyMjY1Ni44OTQ1MzEgMy4zOTA2MjYgMy45NTcwMzEgNS42MjUgNy4yOTY4NzUgNS42MjV6%0D%0AbTAgMCIvPjxwYXRoIGQ9Im0yNDYuODUxNTYyIDE2My4wMDM5MDZjMS40MDIzNDQgMi4zOTQ1MzIg%0D%0AMy45MjU3ODIgMy43NDIxODggNi41MjM0MzggMy43NDIxODggMS4yODkwNjIgMCAyLjU5NzY1Ni0u%0D%0AMzMyMDMyIDMuNzk2ODc1LTEuMDM1MTU2IDMuNjAxNTYzLTIuMTA1NDY5IDQuODIwMzEzLTYuNzIy%0D%0ANjU3IDIuNzE4NzUtMTAuMzI4MTI2LTIuMTA1NDY5LTMuNTkzNzUtNi43MjY1NjMtNC44MTI1LTEw%0D%0ALjMyODEyNS0yLjcwNzAzMS0zLjYwNTQ2OSAyLjEwMTU2My00LjgxMjUgNi43MjI2NTctMi43MTA5%0D%0AMzggMTAuMzI4MTI1em0wIDAiLz48cGF0aCBkPSJtMzc2Ljg2MzI4MSAyNzAuMjkyOTY5YzEuNjkx%0D%0ANDA3IDIuNTc0MjE5IDQuOTMzNTk0IDMuODU5Mzc1IDcuOTI5Njg4IDMuMTcxODc1IDMuMTI4OTA2%0D%0ALS43MjI2NTYgNS41NTA3ODEtMy40MDYyNSA1Ljg1NTQ2OS02LjYyMTA5NC4yOTY4NzQtMy4xNDQ1%0D%0AMzEtMS40Mjk2ODgtNi4yMDcwMzEtNC4yODUxNTctNy41NjI1LTIuNzkyOTY5LTEuMzI0MjE5LTYu%0D%0AMjE4NzUtLjc2NTYyNS04LjQ1MzEyNSAxLjM3MTA5NC0yLjU4OTg0NCAyLjQ4NDM3NS0zLjA4NTkz%0D%0ANyA2LjY2MDE1Ni0xLjA0Njg3NSA5LjY0MDYyNXptMCAwIi8+PHBhdGggZD0ibTUwNC40NDkyMTkg%0D%0AMzk4LjMxMjVoLTUwLjMzMjAzMWMtNC4xNzE4NzYgMC03LjU1MDc4MiAzLjM4MjgxMi03LjU1MDc4%0D%0AMiA3LjU1MDc4MSAwIDguNjAxNTYzLTcgMTUuNjAxNTYzLTE1LjYwMTU2MiAxNS42MDE1NjNoLTEz%0D%0ALjU4OTg0NHYtMzAuNTExNzE5aDI3Ljk4NDM3NWMyMC4yNjk1MzEgMCAzOC43Njk1MzEtOS4zNzEw%0D%0AOTQgNTAuNzYxNzE5LTI1LjcxMDkzNyAxMS45OTYwOTQtMTYuMzM5ODQ0IDE1LjM4NjcxOC0zNi44%0D%0AMDA3ODIgOS4zMDg1OTQtNTYuMTM2NzE5LTExLjUyMzQzOC0zNi42NjQwNjMtNDIuMTI1LTYzLjgw%0D%0ANDY4OC03OS44NTkzNzYtNzAuODMyMDMxLTU0LjIxODc1LTEwLjA5Mzc1LTEyNi4xOTkyMTgtNDMu%0D%0ANjE3MTg4LTE2MS4yMzgyODEtMTQ1Ljk4ODI4Mi0xLjM0NzY1Ni0zLjk0NTMxMi01LjY0MDYyNS02%0D%0ALjA1MDc4MS05LjU4NTkzNy00LjY5OTIxOC0zLjk0NTMxMyAxLjM1MTU2Mi02LjA0Njg3NSA1LjY0%0D%0ANDUzMS00LjY5OTIxOSA5LjU4OTg0MyAxNi45NDkyMTkgNDkuNTE5NTMxIDQzLjkxNDA2MyA4OC40%0D%0AODgyODEgODAuMTQ0NTMxIDExNS44MTI1IDI2LjczMDQ2OSAyMC4xNjQwNjMgNTcuODkwNjI1IDMz%0D%0ALjY2NDA2MyA5Mi42MTMyODIgNDAuMTI4OTA3IDMyLjIzNDM3NCA2LjAwMzkwNiA1OC4zNzUgMjku%0D%0AMTkxNDA2IDY4LjIxODc1IDYwLjUxNTYyNCAzLjU0Njg3NCAxMS4yNzczNDQgMi44MzU5MzcgMjMu%0D%0AMDUwNzgyLTEuNzkyOTY5IDMzLjUzMTI1aC0xODMuMjYxNzE5Yy0xOC45ODA0NjkgMC0zNy45ODgy%0D%0AODEtNC40ODgyODEtNTQuOTY4NzUtMTIuOTgwNDY4bC02MC4wNjY0MDYtMzAuMDMxMjVjLTE5LjA3%0D%0ANDIxOS05LjUzNTE1Ni00MC40MjE4NzUtMTQuNTc4MTI1LTYxLjczNDM3NS0xNC41NzgxMjVoLTI3%0D%0ALjAxNTYyNWMtNC4xNzE4NzUgMC03LjU1MDc4MiAzLjM3ODkwNi03LjU1MDc4MiA3LjU1MDc4MSAw%0D%0AIDQuMTY3OTY5IDMuMzc4OTA3IDcuNTUwNzgxIDcuNTUwNzgyIDcuNTUwNzgxaDI3LjAxNTYyNWMx%0D%0AOC45ODA0NjkgMCAzNy45OTIxODcgNC40ODgyODEgNTQuOTgwNDY5IDEyLjk4NDM3NWw2MC4wNjY0%0D%0AMDYgMzAuMDI3MzQ0YzE5LjA2MjUgOS41MzUxNTYgNDAuNDA2MjUgMTQuNTc4MTI1IDYxLjcyMjY1%0D%0ANiAxNC41NzgxMjVoMTcyLjgwNDY4OGMtOC44NjMyODIgOC43MDMxMjUtMjAuNjc1NzgyIDEzLjU4%0D%0ANTkzNy0zMy40MTQwNjMgMTMuNTg1OTM3aC0xMzkuMzg2NzE5Yy0xOC45ODQzNzUgMC0zNy45OTYw%0D%0AOTQtNC40ODgyODEtNTQuOTc2NTYyLTEyLjk4MDQ2OGwtNjAuMDY2NDA2LTMwLjAzMTI1Yy0xOS4w%0D%0ANzAzMTMtOS41MzUxNTYtNDAuNDE0MDYzLTE0LjU3NDIxOS02MS43MzQzNzYtMTQuNTc0MjE5aC0x%0D%0AMTQuMDkzNzV2LTEzLjU5Mzc1aDU2Ljg3ODkwN2M0LjE2Nzk2OSAwIDcuNTUwNzgxLTMuMzc4OTA2%0D%0AIDcuNTUwNzgxLTcuNTQ2ODc1IDAtNC4xNzE4NzUtMy4zODI4MTItNy41NTA3ODEtNy41NTA3ODEt%0D%0ANy41NTA3ODFoLTU2Ljg3ODkwN3YtNjYuMzg2NzE5YzAtMjMuODA0Njg4IDQuOTYwOTM4LTQ2Ljg3%0D%0ANSAxNC43NDYwOTQtNjguNTc0MjE5IDEwLjY3MTg3NS0yMy42NjAxNTYgMTYuMDg1OTM4LTQ4Ljgy%0D%0AMDMxMiAxNi4wODU5MzgtNzQuNzc3MzQzdi02NC43MzgyODJoMTg2LjE5NTMxMmMyLjMxNjQwNiAx%0D%0AOS45NTMxMjUgNS43NzczNDQgMzguOTUzMTI1IDEwLjMxMjUgNTYuNTcwMzEzIDEuMDM5MDYzIDQu%0D%0AMDM5MDYyIDUuMTU2MjUgNi40Njg3NSA5LjE5MTQwNiA1LjQyOTY4NyA0LjAzOTA2My0xLjAzOTA2%0D%0AMiA2LjQ2ODc1LTUuMTUyMzQ0IDUuNDI5Njg4LTkuMTkxNDA2LTQuODU1NDY5LTE4Ljg3NS04LjQ0%0D%0AMTQwNi0zOS40Mzc1LTEwLjY1MjM0NC02MS4xMjEwOTQtLjM5NDUzMS0zLjg1NTQ2OC0zLjYzNjcx%0D%0AOC02Ljc4NTE1Ni03LjUxMTcxOC02Ljc4NTE1NmgtMjAwLjUxNTYyNmMtNC4xNzE4NzQgMC03LjU1%0D%0AMDc4MSAzLjM4MjgxMi03LjU1MDc4MSA3LjU1MDc4MXY3Mi4yODkwNjNjMCAyMy44MDA3ODEtNC45%0D%0ANjA5MzcgNDYuODcxMDk0LTE0Ljc1IDY4LjU3MDMxMi0xMC42NzE4NzUgMjMuNjYwMTU2LTE2LjA4%0D%0AMjAzMSA0OC44MjAzMTMtMTYuMDgyMDMxIDc0Ljc3NzM0NCAwIDAgLjA1ODU5MzggMTAzLjUzMTI1%0D%0ALjA4MjAzMTIgMTAzLjY4NzUuMDA3ODEyNi4wNjY0MDYgOS43MTg3NDk4IDU3Ljc4MTI1IDkuNzE4%0D%0ANzQ5OCA1Ny43ODEyNS42MDkzNzUgMy42MzI4MTIgMy43NTc4MTMgNi4yOTY4NzUgNy40NDUzMTMg%0D%0ANi4yOTY4NzVoMTcuMjE0ODQ0djMwLjUxMTcxOWgtMjYuOTEwMTU3Yy00LjE3MTg3NSAwLTcuNTUw%0D%0ANzgxIDMuMzc4OTA2LTcuNTUwNzgxIDcuNTUwNzgxdjUwLjMzMjAzMWMwIDQuMTcxODc1IDMuMzc4%0D%0AOTA2IDcuNTUwNzgyIDcuNTUwNzgxIDcuNTUwNzgyaDQyMy40MTQwNjNjNDQuNjgzNTk0IDAgODEu%0D%0AMDM1MTU2LTM2LjM1MTU2MyA4MS4wMzUxNTYtODEuMDM1MTU3IDAtNC4xNjc5NjktMy4zNzg5MDYt%0D%0ANy41NTA3ODEtNy41NTA3ODEtNy41NTA3ODF6bS0xMDIuMTcxODc1LTcuMzU5Mzc1djMwLjUxMTcx%0D%0AOWgtNTQuMzU1NDY5di0zMC41MTE3MTl6bS0xNTguMDM1MTU2LTE0LjU3NDIxOWMxOS4wNjY0MDYg%0D%0AOS41MzUxNTYgNDAuNDE0MDYyIDE0LjU3NDIxOSA2MS43MzA0NjggMTQuNTc0MjE5aDI2Ljg0NzY1%0D%0ANnYzMC41MTE3MTloLTIzMC43NXYtMzAuNTExNzE5aDExLjgwODU5NGMzLjQzMzU5NCAwIDYuNDMz%0D%0ANTk0LTIuMzE2NDA2IDcuMzA0Njg4LTUuNjM2NzE5bDEzLjU4NTkzNy01MS44MDg1OTRjMTcuMTEz%0D%0AMjgxLjc4OTA2MyAzNC4wODU5MzggNS4xNzk2ODggNDkuNDA2MjUgMTIuODM1OTM4em0tMjI3Ljc2%0D%0ANTYyNi00My4wMTE3MThoMTAyLjcxODc1bC0xMS4xNDA2MjQgNDIuNDg0Mzc0aC04NC40MjE4NzZ6%0D%0AbTMzLjA4NTkzOCA1Ny41ODU5MzdoMzcuNDA2MjV2MzAuNTExNzE5aC0zNy40MDYyNXptMzgxLjQw%0D%0AMjM0NCA4MC44NDM3NWgtNDE1Ljg2MzI4MnYtMzUuMjMwNDY5aDQxNS44NjMyODJjMTQuMzI4MTI1%0D%0AIDAgMjYuMzkwNjI1LTkuODYzMjgxIDI5Ljc2MTcxOC0yMy4xNTIzNDRoMzUuNzQyMTg4Yy0zLjc1%0D%0AMzkwNiAzMi44MTY0MDctMzEuNjk5MjE5IDU4LjM4MjgxMy02NS41MDM5MDYgNTguMzgyODEzem0w%0D%0AIDAiLz48L3N2Zz4=',
                    iconSize: [24, 28]
                }),
            markerOption = address => {
                return {
                    icon: myIcon,
                    label: `
                         <div class="hint-content">
                            <span>${address}</span>
                         </div>`
                }
            }

            map = DG.map(id, {
                center: [55.75, 37.62],
                zoom: 11,
                zoomControl: true,
                scrollWheelZoom: false,
                closePopupOnClick: false
            });

            DG.control.location({position: 'topright'}).addTo(map);

            data.forEach(rollerPoint => {

                coordinates[0] = rollerPoint.geoData.coordinates[1]
                coordinates[1] = rollerPoint.geoData.coordinates[0]

                let marker = DG.marker(coordinates, markerOption(rollerPoint.Address))

                let uniqAddressPaid;

                switch(rollerPoint.Address) {
                    case '5-й Лучевой просек, дом 1':
                        uniqAddressPaid = 'платно'
                        break;
                    case 'улица Каретный Ряд, дом 3, строение 1':
                        uniqAddressPaid = 'платно'
                        break;
                    case 'улица Крымский Вал, дом 9, строение 1':
                        uniqAddressPaid = 'платно'
                        break;
                    case 'проспект Мира, дом 119, строение 6':
                        uniqAddressPaid = 'платно'
                        break;
                    case 'Старая Басманная улица, дом 15А, строение 4':
                        uniqAddressPaid = 'платно'
                        break;
                }

                let popup = DG.popup().setContent(`
                    <div class="skating-rinks-popup">
                    <div class="skating-rinks-popup__row-1">
                        <div class="skating-rinks-popup__col-1">
                            <p class="skating-rinks-popup__address">
                                <span>Aдрес:</span>
                                <span>${rollerPoint.Address}</span>
                            </p>
                            <p class="skating-rinks-popup__paid">
                                <span>Стоимость:</span>
                                <span>${uniqAddressPaid == 'платно' ? uniqAddressPaid : rollerPoint.Paid}</span>
                            </p>
                            <p class="skating-rinks-popup__phone">
                                <span>Телефон:</span>
                                <span>${rollerPoint.HelpPhone}</span>
                            </p>
                            <p class="skating-rinks-popup__usage-period">
                                <span>Период работы:</span>
                                <span>${rollerPoint.UsagePeriodWinter}</span>
                            </p>
                        </div>
                        <div class="skating-rinks-popup__col-2">
                            <a
                            class="skating-rinks-popup__photo-box"
                            href="https://op.mos.ru/MEDIA/showFile?id=${rollerPoint.PhotoWinter[0].Photo}">
                                <img
                                class="skating-rinks-popup__photo-img"
                                src="https://op.mos.ru/MEDIA/showFile?id=${rollerPoint.PhotoWinter[0].Photo}"
                                />
                            </a>
                        </div>
                    </div>
                    <div class="skating-rinks-popup__working-hours-box">
                        <ul class="skating-rinks-popup__working-hours-list">
                            <li class="skating-rinks-popup__working-hours-item">
                                <span>${rollerPoint.WorkingHoursWinter[0].DayOfWeek}:</span> <span>${rollerPoint.WorkingHoursWinter[0].Hours}</span>
                            </li>
                            <li class="skating-rinks-popup__working-hours-item">
                                <span>${rollerPoint.WorkingHoursWinter[1].DayOfWeek}:</span> <span>${rollerPoint.WorkingHoursWinter[1].Hours}</span>
                            </li>
                            <li class="skating-rinks-popup__working-hours-item">
                                <span>${rollerPoint.WorkingHoursWinter[2].DayOfWeek}:</span> <span>${rollerPoint.WorkingHoursWinter[2].Hours}</span>
                            </li>
                            <li class="skating-rinks-popup__working-hours-item">
                                <span>${rollerPoint.WorkingHoursWinter[3].DayOfWeek}:</span> <span>${rollerPoint.WorkingHoursWinter[3].Hours}</span>
                            </li>
                            <li class="skating-rinks-popup__working-hours-item">
                                <span>${rollerPoint.WorkingHoursWinter[4].DayOfWeek}:</span> <span>${rollerPoint.WorkingHoursWinter[4].Hours}</span>
                            </li>
                            <li class="skating-rinks-popup__working-hours-item skating-rinks-popup__working-hours-item--weekends">
                                <span>${rollerPoint.WorkingHoursWinter[5].DayOfWeek}:</span> <span>${rollerPoint.WorkingHoursWinter[5].Hours}</span>
                            </li>
                            <li class="skating-rinks-popup__working-hours-item skating-rinks-popup__working-hours-item--weekends">
                                <span>${rollerPoint.WorkingHoursWinter[6].DayOfWeek}:</span> <span>${rollerPoint.WorkingHoursWinter[6].Hours}</span>
                            </li>
                        </ul>
                    </div>
                </div>
                `)

                marker.bindPopup(popup)

                markerGroup.addLayer(marker)

                popup.on('click', e => {
                    if (popup.isOpen()) {

                        this.currentPopup = popup

                        let popupPhotoPreview = document.querySelector('.skating-rinks-popup__photo-box')

                        this.mapPopupPhotoModalOpen(popupPhotoPreview, `https://op.mos.ru/MEDIA/showFile?id=${rollerPoint.PhotoWinter[0].Photo}`)
                    }
                })
            })

            map.addLayer(markerGroup)
        })
    }

    mapPopupPhotoModalOpen(picPreview, picUrl) {

        // picPreview.onclick = () => {
        //     lightGallery((document.getElementById('lightgallery')))
        //     $(picPreview).lightGallery({
        //         selector: '.skating-rinks-popup__photo-img'
        //     });
        // }

        // jQ(picPreview).fancybox({
        //     protect: true,
        //     buttons : [
        //         'zoom',
        //         'close'
        //     ],
        //     baseClass: 'fancy-skating-rinks'
        // });
    }
}

export let Map_SkatingRinks = new MapNY1('skating-rinks')
