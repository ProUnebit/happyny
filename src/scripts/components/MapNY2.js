import { salutesData } from './salutesData'

import DG from '2gis-maps'

class MapNY2 {
    constructor(id) {
        this.id = id
        this.data;
    }

    async mapInit(url) {

        // console.log('map2 init')
        
        this.data = salutesData

        await this.mapMarking(this.id)
    }

    mapGetData(url) {

        return fetch(url).then(res => res.json()).then(data => data)
    }


    mapMarking(id) {

        let { data } = this

        DG.then(() => {

            let map,
            markers = DG.featureGroup(),
            coordinates = [],
            myIcon = DG.icon({
                iconUrl: 'data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pg0KPCEtLSBHZW5lcmF0%0D%0Ab3I6IEFkb2JlIElsbHVzdHJhdG9yIDE5LjAuMCwgU1ZHIEV4cG9ydCBQbHVnLUluIC4gU1ZHIFZl%0D%0AcnNpb246IDYuMDAgQnVpbGQgMCkgIC0tPg0KPHN2ZyB2ZXJzaW9uPSIxLjEiIGlkPSJMYXllcl8x%0D%0AIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8v%0D%0Ad3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4PSIwcHgiIHk9IjBweCINCgkgdmlld0JveD0iMCAwIDUx%0D%0AMi4wMDIgNTEyLjAwMiIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgNTEyLjAwMiA1%0D%0AMTIuMDAyOyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+DQo8Zz4NCgk8cG9seWdvbiBzdHlsZT0iZmls%0D%0AbDojRjk1NDI4OyIgcG9pbnRzPSI0MjcuMDg0LDE3OS4wNjcgMzYyLjQ0OCwxOTcuMDI5IDMyMS4w%0D%0ANjksMjA4LjUyMyAzMzAuNTg5LDE2MC4wOTYgMzY4LjI1NCwxNjcuNTAzIAkiLz4NCgk8cG9seWdv%0D%0AbiBzdHlsZT0iZmlsbDojRjk1NDI4OyIgcG9pbnRzPSI0MTYuOTgzLDIzMC40OSA0MDYuODgyLDI4%0D%0AMS45MDEgMzQyLjIzNSwyOTkuODYyIDMwMC44NTUsMzExLjM1NyAzMTAuOTU2LDI1OS45MzQgDQoJ%0D%0ACTM1Mi4zNDcsMjQ4LjQ0IAkiLz4NCjwvZz4NCjxnPg0KCTxwb2x5Z29uIHN0eWxlPSJmaWxsOiMy%0D%0AQkE1Rjc7IiBwb2ludHM9IjQwNi44ODIsMjgxLjkwMSAzOTYuNzY5LDMzMy4zMTIgMzMyLjEyMiwz%0D%0ANTEuMjczIDI5MC44NDcsMzYyLjc0NCAyOTAuNzQyLDM2Mi43NjggDQoJCTI5MC43NTQsMzYyLjcy%0D%0AMSAzMDAuODQzLDMxMS4zNTcgMzAwLjg1NSwzMTEuMzU3IDM0Mi4yMzUsMjk5Ljg2MiAJIi8+DQoJ%0D%0APHBvbHlnb24gc3R5bGU9ImZpbGw6IzJCQTVGNzsiIHBvaW50cz0iNDI3LjA5NSwxNzkuMDY3IDQx%0D%0ANi45ODMsMjMwLjQ5IDM1Mi4zNDcsMjQ4LjQ0IDMxMC45NTYsMjU5LjkzNCAzMjEuMDY5LDIwOC41%0D%0AMjMgDQoJCTM2Mi40NDgsMTk3LjAyOSA0MjcuMDg0LDE3OS4wNjcgCSIvPg0KPC9nPg0KPGc+DQoJ%0D%0APHBvbHlnb24gc3R5bGU9ImZpbGw6IzIxOTdEODsiIHBvaW50cz0iMzQyLjIzNSwyOTkuODYyIDMz%0D%0ANy45MzksMzIxLjcwMiAzMzIuMTIyLDM1MS4yNzMgMjkwLjg0NywzNjIuNzQ0IDI5MC43NTQsMzYy%0D%0ALjcyMSANCgkJMzAwLjg0MywzMTEuMzU3IDMwMC44NTUsMzExLjM1NyAJIi8+DQoJPHBvbHlnb24g%0D%0Ac3R5bGU9ImZpbGw6IzIxOTdEODsiIHBvaW50cz0iMzYyLjQ0OCwxOTcuMDI5IDM1OC4xNTIsMjE4%0D%0ALjg3OSAzNTIuMzQ3LDI0OC40NCAzMTAuOTU2LDI1OS45MzQgMzIxLjA2OSwyMDguNTIzIAkiLz4N%0D%0ACjwvZz4NCjxnPg0KCTxwb2x5Z29uIHN0eWxlPSJmaWxsOiNFNTQ3Mjg7IiBwb2ludHM9IjMxMC45%0D%0ANTYsMjU5LjkzNCAzNTIuMzQ3LDI0OC40NCAzNDguMDUxLDI3MC4yOSAzNDIuMjM1LDI5OS44NjIg%0D%0AMzAwLjg1NSwzMTEuMzU3IAkiLz4NCgk8cG9seWdvbiBzdHlsZT0iZmlsbDojRTU0NzI4OyIgcG9p%0D%0AbnRzPSIzNjguMjU0LDE2Ny41MDMgMzYyLjQ0OCwxOTcuMDI5IDMyMS4wNjksMjA4LjUyMyAzMzAu%0D%0ANTg5LDE2MC4wOTYgCSIvPg0KPC9nPg0KPGc+DQoJPHBvbHlnb24gc3R5bGU9ImZpbGw6I0Y5NTQy%0D%0AODsiIHBvaW50cz0iMjU2Ljg3NSwzMjUuNDQgMTY4LjY3LDI5Ny42NjggMTIzLjkyMywyODMuNTg0%0D%0AIDExMy4zMzUsMjE4LjA1NSAxNTguMDkzLDIzMi4xNSANCgkJMjQ2LjI5NywyNTkuOTIyIAkiLz4N%0D%0ACgk8cG9seWdvbiBzdHlsZT0iZmlsbDojRjk1NDI4OyIgcG9pbnRzPSIyMzUuNzIsMTk0LjQwNSAx%0D%0ANDcuNTA0LDE2Ni42MzIgMTAyLjc1NywxNTIuNTM3IDkyLjE4LDg3LjAwOCA5Mi4xOTIsODcuMDA4%0D%0AIA0KCQkxMzYuOTI3LDEwMS4wOTEgMjI1LjEzMiwxMjguODc1IAkiLz4NCjwvZz4NCjxnPg0KCTxw%0D%0Ab2x5Z29uIHN0eWxlPSJmaWxsOiNFNTQ3Mjg7IiBwb2ludHM9IjE0Ny41MDQsMTY2LjYzMiAxMDIu%0D%0ANzU3LDE1Mi41MzcgOTIuMTgsODcuMDA4IDkyLjE5Miw4Ny4wMDggMTM2LjkyNywxMDEuMDkxIA0K%0D%0ACQkxNDIuNDE5LDEzNS4xNDUgCSIvPg0KCTxwb2x5Z29uIHN0eWxlPSJmaWxsOiNFNTQ3Mjg7IiBw%0D%0Ab2ludHM9IjE2OC42NywyOTcuNjY4IDEyMy45MjMsMjgzLjU4NCAxMTMuMzM1LDIxOC4wNTUgMTU4%0D%0ALjA5MywyMzIuMTUgMTY2LjUzNCwyODQuNDIgCSIvPg0KPC9nPg0KPGc+DQoJPHBvbHlnb24gc3R5%0D%0AbGU9ImZpbGw6I0Y3QjIzOTsiIHBvaW50cz0iMjU2Ljg3NSwzMjUuNDQgMjMwLjkxNCwzMjkuNjMx%0D%0AIDE4My45ODQsMzM3LjIxMyAxNzUuMjg4LDMzOC42MTggMTMzLjg4NSwzNDUuMzA2IA0KCQkxMjMu%0D%0AOTIzLDI4My41ODQgMTY4LjY3LDI5Ny42NjggCSIvPg0KCTxwb2x5Z29uIHN0eWxlPSJmaWxsOiNG%0D%0AN0IyMzk7IiBwb2ludHM9IjI0Ni4yOTcsMjU5LjkyMiAxNTguMDkzLDIzMi4xNSAxMTMuMzM1LDIx%0D%0AOC4wNTUgMTAyLjc1NywxNTIuNTM3IDE0Ny41MDQsMTY2LjYzMiANCgkJMjM1LjcyLDE5NC40MDUg%0D%0ACSIvPg0KPC9nPg0KPGc+DQoJPHBvbHlnb24gc3R5bGU9ImZpbGw6I0UwOUIyRDsiIHBvaW50cz0i%0D%0AMTU4LjA5MywyMzIuMTUgMTEzLjMzNSwyMTguMDU1IDEwMi43NTcsMTUyLjUzNyAxNDcuNTA0LDE2%0D%0ANi42MzIgMTU0LjQ4MiwyMDkuNzg4IAkiLz4NCgk8cG9seWdvbiBzdHlsZT0iZmlsbDojRTA5QjJE%0D%0AOyIgcG9pbnRzPSIxNzUuMjg4LDMzOC42MTggMTMzLjg4NSwzNDUuMzA2IDEyMy45MjMsMjgzLjU4%0D%0ANCAxNjguNjcsMjk3LjY2OCAJIi8+DQo8L2c+DQo8cG9seWdvbiBzdHlsZT0iZmlsbDojRjk1NDI4%0D%0AOyIgcG9pbnRzPSIyNDguOTQ1LDYxLjY5NyAyMTUuMTcsNjcuMTU0IDkyLjE5Miw4Ny4wMDggOTIu%0D%0AMTgsODcuMDA4IDU4LjQwNSw5Mi40NjUgMTQyLjYyOCw4LjcwNyAiLz4NCjxwb2x5Z29uIHN0eWxl%0D%0APSJmaWxsOiNFNTQ3Mjg7IiBwb2ludHM9IjE0Mi42MjgsOC43MDcgMTM0LjQxNCw4MC4xOTEgNTgu%0D%0ANDA1LDkyLjQ2NSAiLz4NCjxwb2x5Z29uIHN0eWxlPSJmaWxsOiMyQkE1Rjc7IiBwb2ludHM9IjM4%0D%0AOS4zOTYsMTE1LjkyOSA0NTMuNTksMTg0LjI4IDQyNy4wOTUsMTc5LjA2NyA0MjcuMDg0LDE3OS4w%0D%0ANjcgMzMwLjU4OSwxNjAuMDk2IA0KCTMwNC4wOTQsMTU0Ljg4MyAiLz4NCjxwb2x5Z29uIHN0eWxl%0D%0APSJmaWxsOiMyMTk3RDg7IiBwb2ludHM9IjM4OS4zOTYsMTE1LjkyOSAzNjYuMjU4LDE2Ny4xMDgg%0D%0AMzA0LjA5NCwxNTQuODgzICIvPg0KPHBvbHlnb24gc3R5bGU9ImZpbGw6I0Y3QjIzOTsiIHBvaW50%0D%0Acz0iMjE1LjE3LDY3LjE1NCAyMjUuMTMyLDEyOC44NzUgOTIuMTkyLDg3LjAwOCAiLz4NCjxwb2x5%0D%0AZ29uIHN0eWxlPSJmaWxsOiNGOTU0Mjg7IiBwb2ludHM9IjM5Ni43NjksMzMzLjMxMiAzODcuMjQ4%0D%0ALDM4MS43MzkgMzQ4LjcwMiwzNzQuMTU4IDMxMS4xMDcsMzY2Ljc3MyAyOTAuNzQyLDM2Mi43Njgg%0D%0AIi8+DQo8Zz4NCgk8cGF0aCBzdHlsZT0iZmlsbDojMzMzMzMzOyIgZD0iTTI0MC44OTYsMzM2Ljg0%0D%0ANmwxNy4zNjktMi44MDVjMi4yOC0wLjM2OCw0LjMyMS0xLjYyNyw1LjY3My0zLjQ5OQ0KCQljMS4z%0D%0ANTEtMS44NzMsMS45MDQtNC4yMDUsMS41MzYtNi40ODRMMjI1LjE1Niw3NC4zNjJsMjUuMTcxLTQu%0D%0AMDY1YzMuNzMzLTAuNjA0LDYuNjUyLTMuNTQ3LDcuMjIxLTcuMjg2DQoJCWMwLjU2OS0zLjczOS0x%0D%0ALjMzOS03LjQxOC00LjcyMy05LjEwNUwxNDYuNTE4LDAuOTE0Yy0zLjM0My0xLjY2NS03LjM3Ny0x%0D%0ALjAxNS0xMC4wMjYsMS42Mkw1Mi4yNjcsODYuMjkxDQoJCWMtMi42ODIsMi42NjYtMy4zMzYsNi43%0D%0ANTgtMS42MTgsMTAuMTI5YzEuNTAyLDIuOTQ5LDQuNTIyLDQuNzU0LDcuNzU2LDQuNzU0YzAuNDYs%0D%0AMCwwLjkyNS0wLjAzNywxLjM5MS0wLjExMWwyNS4xNzEtNC4wNjUNCgkJbDQwLjMxNywyNDkuNjk0%0D%0AYzAuNjkxLDQuMjc4LDQuMzg4LDcuMzIyLDguNTg2LDcuMzIyYzAuNDYxLDAsMC45MjktMC4wMzcs%0D%0AMS4zOTktMC4xMTNsNDAuMTAyLTYuNDc1DQoJCWMtMC42NzYsNC4xMDgtMi4yOTEsOC4wMzItNC43%0D%0AOTYsMTEuNTAzYy00LjI2Miw1LjkwNC0xMC41NzcsOS43OTctMTcuNzc5LDEwLjk1OWMtNC43NDgs%0D%0AMC43NjctNy45NzUsNS4yMzctNy4yMDgsOS45ODUNCgkJYzAuNjkxLDQuMjc3LDQuMzg4LDcuMzIs%0D%0AOC41ODYsNy4zMmMwLjQ2MSwwLDAuOTI5LTAuMDM3LDEuMzk5LTAuMTEzYzIxLjM5MS0zLjQ1NCwz%0D%0ANi43OTktMjEuNjQyLDM3LjU0NS00Mi41MjFsMzAuNTg1LTQuOTM4DQoJCWwyNi42NTIsMTY1LjA1%0D%0AOGMwLjY5MSw0LjI3OCw0LjM4OCw3LjMyMiw4LjU4Nyw3LjMyMmMwLjQ2LDAsMC45MjktMC4wMzcs%0D%0AMS4zOTgtMC4xMTNjNC43NDktMC43NjcsNy45NzUtNS4yMzcsNy4yMDktOS45ODUNCgkJTDI0MC44%0D%0AOTYsMzM2Ljg0NnogTTEyNC4xODEsMjMwLjYwMmwxMTQuMzc2LDM2LjAxMmw3LjQ3NCw0Ni4yODZs%0D%0ALTExNC4zNzYtMzYuMDEyTDEyNC4xODEsMjMwLjYwMnogTTIyNC44NzEsMTgxLjg1NQ0KCQlsLTEx%0D%0ANC4zNzYtMzYuMDExbC03LjQ3NC00Ni4yODZsMTE0LjM3NiwzNi4wMTFMMjI0Ljg3MSwxODEuODU1%0D%0AeiBNMjI3Ljk3NywyMDEuMDkxbDcuNDc0LDQ2LjI4NmwtMTE0LjM3Ni0zNi4wMTJsLTcuNDc0LTQ2%0D%0ALjI4Ng0KCQlMMjI3Ljk3NywyMDEuMDkxeiBNMjE0LjI5MiwxMTYuMzMybC04NC40MzEtMjYuNTgz%0D%0AbDc4LjEwMi0xMi42MTFMMjE0LjI5MiwxMTYuMzMyeiBNMTQ0LjM0MSwxOS4yODlsNzYuNDksMzgu%0D%0AMTI5TDgzLjczOSw3OS41NTUNCgkJTDE0NC4zNDEsMTkuMjg5eiBNMTM0Ljc2LDI5Ni4xMjVsODQu%0D%0ANDMyLDI2LjU4NGwtNzguMTA0LDEyLjYxMUwxMzQuNzYsMjk2LjEyNXoiLz4NCgk8cGF0aCBzdHls%0D%0AZT0iZmlsbDojMzMzMzMzOyIgZD0iTTQ1OS45NDEsMTc4LjMyMWwtNjQuMjAxLTY4LjM0OGMtMi41%0D%0ANTgtMi43MjMtNi41NjctMy41MTEtOS45NjQtMS45NmwtODUuMzAxLDM4Ljk1Mw0KCQljLTMuNDQs%0D%0AMS41NzEtNS40NzMsNS4xODMtNS4wMzEsOC45MzljMC40NDEsMy43NTYsMy4yNTgsNi43OTcsNi45%0D%0ANjcsNy41MjZsMTcuOTUyLDMuNTNsLTM4LjE2OSwxOTQuMTI0DQoJCWMtMC40NDYsMi4yNjYsMC4w%0D%0AMjcsNC42MTYsMS4zMTUsNi41MzNjMS4yODYsMS45MTcsMy4yODMsMy4yNDUsNS41NDksMy42OWwx%0D%0AMS44MywyLjMyNmwtMjUuMTEyLDEyNy43MTQNCgkJYy0wLjkyOCw0LjcxOCwyLjE0Niw5LjI5Niw2%0D%0ALjg2NCwxMC4yMjRjMC41NjgsMC4xMTEsMS4xMzMsMC4xNjYsMS42ODksMC4xNjZjNC4wNzgsMCw3%0D%0ALjcxOS0yLjg3OCw4LjUzNi03LjAyOWwyNS4xMTItMTI3LjcxNA0KCQlsMjEuMzQ2LDQuMTk3YzAu%0D%0AMDM0LDIwLjg5MiwxNC44MTQsMzkuNTk1LDM2LjA3NSw0My43NzVjMC41NjgsMC4xMTEsMS4xMzMs%0D%0AMC4xNjYsMS42ODksMC4xNjZjNC4wNzgsMCw3LjcxOS0yLjg3OCw4LjUzNi03LjAzDQoJCWMwLjky%0D%0AOC00LjcxOC0yLjE0Ni05LjI5Ni02Ljg2NC0xMC4yMjRjLTExLjc3NS0yLjMxNS0yMC4yODgtMTEu%0D%0AOTI5LTIxLjc4MS0yMy4yMTZsMjguNTkxLDUuNjIyDQoJCWMwLjU2OCwwLjExMSwxLjEzMywwLjE2%0D%0ANiwxLjY4OSwwLjE2NmM0LjA3OCwwLDcuNzE5LTIuODc4LDguNTM2LTcuMDNsMzguMTY5LTE5NC4x%0D%0AMjNsMTcuOTUyLDMuNTMNCgkJYzAuNTYyLDAuMTEsMS4xMjUsMC4xNjUsMS42ODIsMC4xNjVjMy4x%0D%0AMjIsMCw2LjA2NC0xLjY4NSw3LjYxOC00LjQ5MkM0NjMuMDQ1LDE4NS4xODksNDYyLjUyOSwxODEu%0D%0AMDc3LDQ1OS45NDEsMTc4LjMyMXoNCgkJIE0zODcuMzI1LDEyNi40NTFsNDEuMzkxLDQ0LjA2NWwt%0D%0AOTYuMzg2LTE4Ljk1MkwzODcuMzI1LDEyNi40NTF6IE0zODkuMjYzLDMyNi4zNTdsLTg3LjI1Nywy%0D%0ANC4yNGw2LjM1LTMyLjI5Mmw4Ny4yNTctMjQuMjQNCgkJTDM4OS4yNjMsMzI2LjM1N3ogTTM5OS4z%0D%0ANzIsMjc0Ljk0NWwtODcuMjU3LDI0LjIzOWw2LjM1LTMyLjI5Mmw4Ny4yNTYtMjQuMjM5TDM5OS4z%0D%0ANzIsMjc0Ljk0NXogTTQwOS40OCwyMjMuNTMzbC04Ny4yNTYsMjQuMjM5DQoJCWw2LjM1LTMyLjI5%0D%0AMmw4Ny4yNTYtMjQuMjM5TDQwOS40OCwyMjMuNTMzeiBNMzg5LjM0LDE4MC41MjNsLTU3LjAwNywx%0D%0ANS44MzdsNS4xMi0yNi4wMzlMMzg5LjM0LDE4MC41MjN6IE0zMjguNDk2LDM2MS4zMTQNCgkJbDU3%0D%0ALjAwNy0xNS44MzdsLTUuMTIsMjYuMDM5TDMyOC40OTYsMzYxLjMxNHoiLz4NCjwvZz4NCjxnPg0K%0D%0APC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwv%0D%0AZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+%0D%0ADQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjwvc3ZnPg0K',
                iconSize: [30, 34]
            }),
            markerOption = address => {
                return {
                    icon: myIcon,
                    label: `
                    <div class="hint-content">
                       <span>${address}</span>
                    </div>`
                }
            };

            map = DG.map(id, {
                center: [55.75, 37.62],
                zoom: 11,
                zoomControl: true,
                scrollWheelZoom: false,
                closePopupOnClick: false
            })

            DG.control.location({position: 'topright'}).addTo(map);

            data.forEach(salutePoint => {

                coordinates[0] = salutePoint.geoData.coordinates[1]
                coordinates[1] = salutePoint.geoData.coordinates[0]

                let marker = DG.marker(coordinates, markerOption(salutePoint.Location))

                let uniqAddressName;
                let uniqAddressLocation;
                let uniqAddressLocationClarification;

                switch(salutePoint.Name) {
                    case 'Праздничный Фейерверк в Печатниках':
                        uniqAddressName = 'Праздничный фейерверк в Печатниках'
                        uniqAddressLocation = 'напротив улицы Гурьянова, дом 41А'
                        uniqAddressLocationClarification = 'набережная Москвы-реки'
                        break;
                    default:
                        salutePoint.LocationClarification ?
                        uniqAddressLocationClarification = salutePoint.LocationClarification
                        :
                        uniqAddressLocationClarification = ''

                }

                let popup =  DG.popup().setContent(`
                    <div class="salutes-popup">
                        <p class="salutes-popup__name">
                            <span>Наименование:</span>
                            <span>${
                                salutePoint.Name == 'Праздничный Фейерверк в Печатниках' ?
                                uniqAddressName
                                :
                                salutePoint.Name
                            }</span>
                        </p>
                        <p class="salutes-popup__address">
                            <span>Улица:</span>
                            <span>${
                                salutePoint.Name == 'Праздничный Фейерверк в Печатниках' ?
                                uniqAddressLocation
                                :
                                salutePoint.Location
                            }</span>
                        </p>
                        <p class="salutes-popup__desc">
                            ${
                                uniqAddressLocationClarification != '' ?
                                '<span>Описание:</span>'+'<span>'+uniqAddressLocationClarification+'</span>'
                                :
                                ''
                            }
                        </p>
                    </div>
                    `)

                marker.addTo(markers).bindPopup(popup)
             })

            markers.addTo(map)
        })
    }
}

export let Map_Salutes = new MapNY2('salutes')
