import { socNetShareGameBefore, socNetShareGameAfter } from '../partials/socNetShare'
import 'sharer.js'

class GameNY {
    constructor(id, gameDataArr, resultDataObj) {
        // ID
        this.id = id
        // Data
        this.gameDataArr = gameDataArr
        this.resultDataObj = resultDataObj
        // Sections
        this.motherGameContainer
        this.gameIntroBox
        this.gameGameplayBox
        this.gameResultBox
        // Values
        this.gameStep
        this.gameCorrectAnswers
        this.gameInTotalSteps
        this.gameDataQuestionsSequence
    }

    gameInit() {

        // console.log('game init')

        this.gameStep = 0
        this.gameCorrectAnswers = 0
        this.gameInTotalSteps = this.gameDataArr.length

        this.motherGameContainer = document.getElementById(`game--${this.id}`)
        this.gameIntroBox = this.motherGameContainer.querySelector(`.game--${this.id}__intro`)
        this.gameGameplayBox = this.motherGameContainer.querySelector(`.game--${this.id}__gameplay`)
        this.gameResultBox = this.motherGameContainer.querySelector(`.game--${this.id}__result`)

        this.gameIntroPhaseActive()
    }
    gameClear() {

        this.gameStep = 0
        this.gameCorrectAnswers = 0
        this.gameDataQuestionsSequence = []
    }


    gameIntroPhaseActive() {

        let gameStartButton = this.gameIntroBox.querySelector('.controls-box__game-start-btn')

        socNetShareGameBefore()

        this.gameQuestionCounterController()

        gameStartButton.onclick = () => {
            this.gameIntroBoxDisplaying()
            this.gameGameplayBoxDisplaying()
            this.gameGameplayPhaseActive()
            this.gameQuestionGenerator(this.gameDataQuestionsSequence)
            this.gameMechanicGameplay()
        }
    }
    gameGameplayPhaseActive() {

        let gameNextButton = this.gameGameplayBox.querySelector('.controls-box__game-next-btn')
        let gameShowResulButton = this.gameGameplayBox.querySelector('.controls-box__game-show-result-btn')
        let gameAnswersVariantList = this.gameGameplayBox.querySelector('.question-box__answers-variants__container')

        let innerNextButtonMechanic = () => {

            this.gameQuestionGenerator(this.gameDataQuestionsSequence)
            this.gameMechanicGameplay()

            gameAnswersVariantList.classList.toggle('game-actions__answers-variants-list-up')
        }

        let innerResultButtonMechanic = () => {
            this.gameGameplayBoxDisplaying()
            this.gameResultBoxDisplaying()
            this.gameResultPhaseActive()
        }

        gameNextButton.onclick = innerNextButtonMechanic
        gameShowResulButton.onclick = innerResultButtonMechanic
    }
    gameResultPhaseActive() {

        let gameReloadButton = this.gameResultBox.querySelector('.controls-box__game-reload-btn')
        let gameAnswersVariantList = this.gameGameplayBox.querySelector('.question-box__answers-variants__container')

        this.gameShowResultInfo()

        socNetShareGameAfter()

        gameReloadButton.onclick = () => {

            gameAnswersVariantList.classList.toggle('game-actions__answers-variants-list-up')

            this.gameClear()
            this.gameShowResultButtonDisplayingOff()
            this.gameNextButtonDisplayingOn()
            this.gameResultBoxDisplaying()
            this.gameGameplayBoxDisplaying()
            this.gameQuestionCounterController()
            this.gameGameplayPhaseActive()
            this.gameQuestionGenerator(this.gameDataQuestionsSequence)
            this.gameMechanicGameplay()
        }
    }


    gameIntroBoxDisplaying() {
        this.gameIntroBox.classList.toggle('game-actions__displaying-off')
    }
    gameGameplayBoxDisplaying() {
        this.gameGameplayBox.classList.toggle('game-actions__displaying-on')
    }
    gameResultBoxDisplaying() {
        this.gameResultBox.classList.toggle('game-actions__displaying-on')
    }
    gameNextButtonDisplayingOn() {
        let gameNextButton = this.gameGameplayBox.querySelector('.controls-box__game-next-btn')
        gameNextButton.classList.remove('game-actions__displaying-off')
        gameNextButton.classList.add('game-actions__displaying-on')
    }
    gameNextButtonDisplayingOff() {
        let gameNextButton = this.gameGameplayBox.querySelector('.controls-box__game-next-btn')
        gameNextButton.classList.remove('game-actions__displaying-on')
        gameNextButton.classList.add('game-actions__displaying-off')
    }
    gameShowResultButtonDisplayingOn() {
        let gameShowResulButton = this.motherGameContainer.querySelector('.controls-box__game-show-result-btn')
        gameShowResulButton.classList.remove('game-actions__displaying-off--flex')
        gameShowResulButton.classList.add('game-actions__displaying-on--flex')
    }
    gameShowResultButtonDisplayingOff() {
        let gameShowResulButton = this.motherGameContainer.querySelector('.controls-box__game-show-result-btn')
        gameShowResulButton.classList.remove('game-actions__displaying-on--flex')
        gameShowResulButton.classList.add('game-actions__displaying-off--flex')
    }
    gameFrameDescBoxDisplayingOn() {
        let gameFrameDescBox = this.gameGameplayBox.querySelector('.question-box__pic-frame__desc')
        let gameFrameDescLines = Array.from(this.gameGameplayBox.querySelectorAll('.question-box__pic-frame__info-line'))

        gameFrameDescLines.forEach(gameFrameDescLine => {
            gameFrameDescLine.style.opacity = '1'
        })
        gameFrameDescBox.style.opacity = '1'
        gameFrameDescBox.style.transform = 'translateY(0)'
    }
    gameFrameDescBoxDisplayingOff() {
        let gameFrameDescBox = this.gameGameplayBox.querySelector('.question-box__pic-frame__desc')
        let gameFrameDescLines = Array.from(this.gameGameplayBox.querySelectorAll('.question-box__pic-frame__info-line'))

        gameFrameDescLines.forEach(gameFrameDescLine => {
            gameFrameDescLine.style.opacity = ''
        })
        gameFrameDescBox.style.opacity = ''
        gameFrameDescBox.style.transform = ''
    }


    helperRandomInteger(min, max) {
        let rand = min + Math.random() * (max + 1 - min)
        rand = Math.floor(rand)
        return rand;
    }
    helperFindIndex(array, value) {
        if (array.indexOf) {
            return array.indexOf(value);
        }
        for (var i = 0; i < array.length; i++) {
            if (array[i] === value) return i;
        }
        return -1;
    }


    gameQuestionCounterController() {

        let {
            gameDataArr,
        } = this

        let randomDataObjNum;
        let randomDataObjArr = []

        while (randomDataObjArr.length < gameDataArr.length) {

            randomDataObjNum = this.helperRandomInteger(1, gameDataArr.length) - 1

            if (this.helperFindIndex(randomDataObjArr, randomDataObjNum) === -1) {
                randomDataObjArr.push(randomDataObjNum)
            }
        }

        this.motherGameContainer.querySelector('.steps-box__in-total').textContent = this.gameInTotalSteps

        this.gameDataQuestionsSequence = randomDataObjArr
    }


    gameQuestionGenerator(questionsSequence) {

        let {
            motherGameContainer,
            gameIntroBox,
            gameGameplayBox,
            gameResultBox,
            gameDataArr
        } = this

        let
        gameFramePic = motherGameContainer.querySelector('.question-box__pic-frame__pic'),
        gameFrameDescLines = Array.from(motherGameContainer.querySelectorAll('.question-box__pic-frame__info-line')),
        gameAnswersVariantsItems = Array.from(motherGameContainer.querySelectorAll('.question-box__answers-variants__item')),
        gameCurrentStep = motherGameContainer.querySelector('.steps-box__current');

        let curI;

        let copyCurrentDataObj = Object.assign({}, gameDataArr[questionsSequence[this.gameStep]])
        copyCurrentDataObj.answers = [...gameDataArr[questionsSequence[this.gameStep]].answers]

        // Generate
        gameCurrentStep.textContent = this.gameStep + 1

        gameFramePic.src = copyCurrentDataObj.frame

        gameFrameDescLines[0].textContent = `Кадр из фильма "${copyCurrentDataObj.description_1}"`
        gameFrameDescLines[1].textContent = copyCurrentDataObj.description_2
        gameFrameDescLines[2].textContent = copyCurrentDataObj.description_3
        gameFrameDescLines[3].textContent = copyCurrentDataObj.description_4

        curI = this.helperRandomInteger(1, 4) - 1
        gameAnswersVariantsItems[0].textContent =
        copyCurrentDataObj.answers[curI]
        copyCurrentDataObj.answers.splice(curI, 1)
        curI = this.helperRandomInteger(1, 3) - 1
        gameAnswersVariantsItems[1].textContent =
        copyCurrentDataObj.answers[curI]
        copyCurrentDataObj.answers.splice(curI, 1)
        curI = this.helperRandomInteger(1, 2) - 1
        gameAnswersVariantsItems[2].textContent =
        copyCurrentDataObj.answers[curI]
        copyCurrentDataObj.answers.splice(curI, 1)
        curI = this.helperRandomInteger(1, 1) - 1
        gameAnswersVariantsItems[3].textContent =
        copyCurrentDataObj.answers[curI]
        copyCurrentDataObj.answers.splice(curI, 1)


        this.gameStepsCounter()
    }

    gameStepsCounter() {
        this.gameStep++
    }
    gameCorrectAnswersCounter() {
        this.gameCorrectAnswers++
    }

    gameMechanicGameplay() {

        let
        gameAnswersVariants = Array.from(this.gameGameplayBox.querySelectorAll('.question-box__answers-variants__item')),
        gameAnswersVariantsDeep = Array.from(this.gameGameplayBox.querySelectorAll('.question-box__answers-variants__item')),
        gameFrameDescBox = this.gameGameplayBox.querySelector('.question-box__pic-frame__desc'),
        gameAnswersVariantList = this.gameGameplayBox.querySelector('.question-box__answers-variants__container'),
        gamePicFrameDesc = this.gameGameplayBox.querySelector('.question-box__pic-frame__desc');

        this.gameFrameDescBoxDisplayingOff()
        this.gameShowResultButtonDisplayingOff()
        this.gameNextButtonDisplayingOff()

        let innerMechanicGameplay = answerVariant => {
            answerVariant.onclick = () => {

                this.gameFrameDescBoxDisplayingOn()

                if (
                    answerVariant.textContent
                    ===
                    this.gameDataArr[this.gameDataQuestionsSequence[this.gameStep - 1]].description_1
                ) {
                        answerVariant.classList.add('game-actions__correct-answer')
                        this.gameCorrectAnswersCounter()
                    }
                else if (
                    answerVariant.textContent
                    !==
                    this.gameDataArr[this.gameDataQuestionsSequence[this.gameStep - 1]].description_1
                ) {
                    answerVariant.classList.add('game-actions__wrong-answer')
                    gameAnswersVariantsDeep.forEach(answerVariantDeep => {
                        if (
                            answerVariantDeep.textContent
                            ===
                            this.gameDataArr[this.gameDataQuestionsSequence[this.gameStep - 1]].description_1
                        ) {
                            answerVariantDeep.classList.add('game-actions__correct-answer')
                        }
                    })
                }
                gameAnswersVariants.forEach(answerVariant => {
                    answerVariant.onclick = null
                })

                if (this.gameStep < this.gameInTotalSteps) {
                    gameAnswersVariantList.classList.toggle('game-actions__answers-variants-list-up')
                    setTimeout(() => this.gameNextButtonDisplayingOn(), 200)
                } else if (this.gameStep >= this.gameInTotalSteps) {
                    gameAnswersVariantList.classList.toggle('game-actions__answers-variants-list-up')
                    this.gameShowResultButtonDisplayingOn()
                }
            }
        }

        gameAnswersVariants.forEach(answerVariant => {

            answerVariant.classList.remove('game-actions__correct-answer')
            answerVariant.classList.remove('game-actions__wrong-answer')

            innerMechanicGameplay(answerVariant)
        })
    }

    gameShowResultInfo() {

        let
        gameCorrectAnswersSpan = this.gameResultBox.querySelector('.results-values__correct-answers'),
        gameInTotalAnswersSpan = this.gameResultBox.querySelector('.results-values__in-total-answers'),
        gameResultDescParagraph = this.gameResultBox.querySelector('.info-box__desc');

        gameCorrectAnswersSpan.textContent = this.gameCorrectAnswers
        gameInTotalAnswersSpan.textContent = this.gameInTotalSteps

        if (this.gameCorrectAnswers <= 3) {
            gameResultDescParagraph.textContent = this.resultDataObj.bad.text
            this.gameResultBox.style.backgroundImage = `url(${this.resultDataObj.bad.frame})`
        } else if (this.gameCorrectAnswers > 3 && this.gameCorrectAnswers < 8) {
            gameResultDescParagraph.textContent = this.resultDataObj.good.text
            this.gameResultBox.style.backgroundImage = `url(${this.resultDataObj.good.frame})`
            // gameResultDescParagraph.style.fontSize = '18px'
        } else if (this.gameCorrectAnswers >= 8) {
            gameResultDescParagraph.textContent = this.resultDataObj.wonderful.text
            this.gameResultBox.style.backgroundImage = `url(${this.resultDataObj.wonderful.frame})`
        }
    }
}

export let Game_Movies = new GameNY(
    'movies',
    [
        {
            frame: 'https://b1.m24.ru/c/1158203.730xp.jpg',
            description_1: 'Реальная любовь',
            description_2: 'Режиссёр: Ричард Кёртис',
            description_3: 'Сценарий: Ричард Кёртис',
            description_4: 'Производство: Universal Picture',
            answers: [
                'Реальная любовь',
                'Четыре рождества',
                'Семьянин',
                'Пережить Рождество'
            ]
        },
        {
            frame: 'https://b1.m24.ru/c/1158206.730xp.jpg',
            description_1: 'Карнавальная ночь',
            description_2: 'Режиссёр: Эльдар Рязанов',
            description_3: 'Сценарий: Борис Ласкин, Владимир Поляков',
            description_4: 'Производство: Мосфильм',
            answers: [
                'Карнавальная ночь',
                'Морозко',
                'Старый новый год',
                'Приходи на меня посмотреть'
            ]
        },
        {
            frame: 'https://b1.m24.ru/c/1158205.730xp.jpg',
            description_1: 'Гринч - похититель Рождества',
            description_2: 'Режиссёр: Рон Ховард',
            description_3: 'Сценарий: Джеффри Прайс, Питер С. Симан',
            description_4: 'Производство: Universal Pictures, Imagine Entertainment, LUNI Productions GmbH and Company KG',
            answers: [
                'Гринч - похититель Рождества',
                'Щелкунчик',
                'Рождество',
                'Эльф'
            ]
        },
        {
            frame: 'https://b1.m24.ru/c/1158201.730xp.jpg',
            description_1: 'Рождественские каникулы',
            description_2: 'Режиссёр: Джеремия Чечик',
            description_3: 'Сценарий: Джон Хьюз',
            description_4: 'Производство: Warner Brothers',
            answers: [
                'Рождественские каникулы',
                'Подарок на Рождество',
                'Рождество с неудачниками',
                'Чудо на 34-й улице'
            ]
        },
        {
            frame: 'https://b1.m24.ru/c/1158207.730xp.jpg',
            description_1: 'Плохой Санта',
            description_2: 'Режиссёр: Терри Цвигофф',
            description_3: 'Сценарий: Гленн Фикарра, Джон Рекуа',
            description_4: 'Производство: Dimension Films, Columbia Pictures',
            answers: [
                'Плохой Санта',
                'Санта Клаус',
                'Фред Клаус, брат Санты',
                'Черная Молния'
            ]
        },
        {
            frame: 'https://b1.m24.ru/c/1158208.730xp.jpg',
            description_1: 'Ёлки',
            description_2: 'Режиссёр: Тимур Бекмамбетов, Александр Войтинский, Дмитрий Киселёв, Александр Андрющенко, Ярослав Чеважевский, Игнас Йонинас',
            description_3: 'Сценарий: Тимур Бекмамбетов',
            description_4: 'Производство: Базелевс',
            answers: [
                'Ёлки',
                'Ирония судьбы. Продолжение',
                'С новым годом, мамы!',
                'Выкрутасы'
            ]
        },
        {
            frame: 'https://b1.m24.ru/c/1158204.730xp.jpg',
            description_1: 'Тариф Новогодний',
            description_2: 'Режиссёр: Евгений Бедарев',
            description_3: 'Сценарий: Елена Ласкарева, Анастасия Волкова',
            description_4: 'Производство: ПРОФИТ, Мосфильм',
            answers: [
                'Тариф Новогодний',
                'Ёлки 2',
                'С новым годом, мамы!',
                'Чёрная Молния'
            ]
        },
        {
            frame: 'https://b1.m24.ru/c/1158200.730xp.jpg',
            description_1: 'Один дома',
            description_2: 'Режиссёр: Крис Коламбус',
            description_3: 'Сценарий: Джон Хьюз',
            description_4: 'Производство: Hughes  Entertainment',
            answers: [
                'Один дома',
                'Рождественское приключение Бетховена',
                'Подарок на Рождество',
                'Отпуск по обмену'
            ]
        },
        {
            frame: 'https://b1.m24.ru/c/1158202.730xp.jpg',
            description_1: 'Ирония судьбы, или С лёгким паром!',
            description_2: 'Режиссёр: Эльдар Рязанов',
            description_3: 'Сценарий: Эмиль Брагинский, Эльдар Рязанов',
            description_4: 'Производство: Киностудия "Мосфильм", Творческое объединение телевизионных фильмов',
            answers: [
                'Ирония судьбы, или С лёгким паром!',
                'Карнавальная ночь',
                'Морозко',
                'Старый новый год'
            ]
        },
        {
            frame: 'https://b1.m24.ru/c/1158209.730xp.jpg',
            description_1: 'Четыре Рождества',
            description_2: 'Режиссёр: Сет Гордон',
            description_3: 'Сценарий: Мэтт Аллен, Калеб Уилсон, Джон Лукас, Скотт Мур',
            description_4: 'Производство: Birnbaum/Barber Production, Wild West Picture Show, Type A Films, New Line Cinema, Spyglass Entertainment',
            answers: [
                'Четыре Рождества',
                'Реальная любовь',
                'Пережить Рождество',
                'Рождественская история'
            ]
        }
    ],
    {
        bad: {
            frame: 'https://b1.m24.ru/c/1158212.730xp.jpg',
            text: 'Ваш результат не дотягивает даже до «среднечка», но это даже хорошо. Все праздники вы можете смотреть фильмы про Рождество или Новый год. Наш совет - возьмите побольше оливье, лягте на диван и включите кнопку “play”, только не забудьте, что 8 января каникулы заканчиваются.'
        },
        good: {
            frame: 'https://b1.m24.ru/c/1158199.730xp.jpg',
            text: 'Скажем честно, вы могли бы и лучше, но жизнь распорядилась так, вы застряли посередине. У вас есть неплохая база, но ее нужно ещё подтягивать. Надеюсь, вы записали фильмы, в которых ошиблись и восполните пробел на праздниках. Но не забывайте и о других радостях этих дней: лыжи, родственники, салатики.'
        },
        wonderful: {
            frame: 'https://b1.m24.ru/c/1158211.730xp.jpg',
            text: 'Видимо вы фанат фильмов про снег, холод, праздничные ужины и рождественские приключения. Вы знаете все или почти все, вы как Стивен Хокинг, только в мире кино. В этом разделе вам делать больше нечего, советуем посмотреть другие рубрики, например, «Праздник на столе» или «Развлечения».'
        }
    }
)
