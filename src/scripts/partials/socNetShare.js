let gameURL = 'https://www.m24.ru/special/NY2019' + '/#game'

export const socNetShareGameBefore = () => {
    // console.log(gameURL)
    // VK
    document.querySelector('.soc-net-share-box__item--vk--before-game').addEventListener('click', function() {
        window.open(`http://vk.com/share.php?url=${encodeURIComponent(gameURL)}&title=${'Игра: Угадай новогодний фильм по кадру'}`, '', 'left=0,top=0,width=600,height=300,menubar=no,toolbar=no,resizable=yes,scrollbars=yes')
    })
    // FA
    document.querySelector('.soc-net-share-box__item--fa--before-game').setAttribute('data-url', gameURL)
    document.querySelector('.soc-net-share-box__item--fa--before-game').setAttribute('data-hashtag', 'новыйгод')
    document.querySelector('.soc-net-share-box__item--fa--before-game').setAttribute('data-title', 'Игра: Угадай новогодний фильм по кадру')
    // TW
    document.querySelector('.soc-net-share-box__item--tw--before-game').addEventListener('click', function() {
        window.open(`https://twitter.com/intent/tweet?url=${encodeURIComponent(gameURL)}&text=${'Игра: Угадай новогодний фильм по кадру'}&hashtags=${'новыйгод, кино, москва24'}`, '', 'left=0,top=0,width=600,height=300,menubar=no,toolbar=no,resizable=yes,scrollbars=yes')
    })
    // TE
    document.querySelector('.soc-net-share-box__item--te--before-game').addEventListener('click', function() {
        window.open(`https://t.me/share/url?url=${encodeURIComponent(gameURL)}&text=${'Игра: Угадай новогодний фильм по кадру'}`, '', 'left=0,top=0,width=600,height=300,menubar=no,toolbar=no,resizable=yes,scrollbars=yes')
    })
}

export const socNetShareGameAfter = () => {
    let
    gameCorrectAnswersSpan = document.querySelector('.results-values__correct-answers').textContent,
    gameInTotalAnswersSpan = document.querySelector('.results-values__in-total-answers').textContent;

    // VK
    document.querySelector('.soc-net-share-box__item--vk--after-game').onclick = function() {
        window.open(`http://vk.com/share.php?url=${encodeURIComponent(gameURL)}&title=${'Игра: Угадай новогодний фильм по кадру. Мой результат: '+gameCorrectAnswersSpan+' из '+gameInTotalAnswersSpan}`, '', 'left=0,top=0,width=600,height=300,menubar=no,toolbar=no,resizable=yes,scrollbars=yes')
    }
    // FA
    document.querySelector('.soc-net-share-box__item--fa--after-game').setAttribute('data-url', gameURL)
    document.querySelector('.soc-net-share-box__item--fa--after-game').setAttribute('data-hashtag', 'новыйгод')
    document.querySelector('.soc-net-share-box__item--fa--before-game').setAttribute('data-title', `Игра: Угадай новогодний фильм по кадру. Мой результат: ${gameCorrectAnswersSpan} из ${gameInTotalAnswersSpan}`)
    // TW
    document.querySelector('.soc-net-share-box__item--tw--after-game').onclick = function() {
        window.open(`https://twitter.com/intent/tweet?url=${encodeURIComponent(gameURL)}&text=${'Игра: Угадай новогодний фильм по кадру. Мой результат: '+gameCorrectAnswersSpan+' из '+gameInTotalAnswersSpan}&hashtags=${gameCorrectAnswersSpan+'из'+gameInTotalAnswersSpan+', '+'новыйгод, кино, москва24'}`, '', 'left=0,top=0,width=600,height=300,menubar=no,toolbar=no,resizable=yes,scrollbars=yes')
    }
    // TE
    document.querySelector('.soc-net-share-box__item--te--after-game').onclick = function() {
        window.open(`https://t.me/share/url?url=${encodeURIComponent(gameURL)}&text=${'Игра: Угадай новогодний фильм по кадру. Мой результат: '+gameCorrectAnswersSpan+' из '+gameInTotalAnswersSpan}`, '', 'left=0,top=0,width=600,height=300,menubar=no,toolbar=no,resizable=yes,scrollbars=yes')
    }
}
